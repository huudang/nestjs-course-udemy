export enum TaskStatus {
  TODO = 'TO DO',
  IN_PROGRESS = 'IN_PROGRESS',
  DONE = 'DONE'
}